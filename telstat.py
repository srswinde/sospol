#!/usr/bin/python

import time
import datetime
import os
try:
	from telescope import kuiper; tel=kuiper()
	from telescope import telescope; tel_slow = telescope('10.30.5.69', 'BIG61')
except Exception :
	raise Exception("Can not communicate with telescope!")

def fake_spol():
	for a in range(100):
		print a
		if os.path.exists('status.tel'):
			os.remove('status.tel')
		fd = open('getstat.loc', 'w')
		fd.write('')
		fd.close()
		time.sleep(1.0)
		if not os.path.exists('status.tel'):
			print "no statusfile!!!!"
			raw_input()
			continue	
		st  = open('status.tel').read()
		if not ( "airmass" in st):
			print "No airmass"
		time.sleep(2.0)

def fake_telstat():
	getfile = "getstat.loc"   
	stopfile = "stopstat.loc" 
	statfile = "status.tel"   
	variable = "airmass"      
	logfile = 'airmass.log'
	
	logfd = open(logfile, 'a')
	ii = 0
	while 1:
		if os.path.exists(getfile):
			
			write_statfile(statfile)
			try:
				os.remove(getfile)
			except Exception as err:
				print err
			out = "{:04}\t{:0.2f}\t{}\n".format(ii, getairmass(), datetime.datetime.utcnow().isoformat()  ) 
			try:
				logfd.write(out )
			except Exception as err:
				print err
				logfd = open(logfile, 'a')
			logfd.flush()
			ii+=1
		elif not os.path.exists(statfile):
			write_statfile(statfile)
		time.sleep(0.01)


def write_statfile(statfile):
	
	try:
		airmass = float( tel.reqALL()['secz'] )
	except Exception as err:
		print err, 'Will write bogus airmass'
		airmass = 1.0
	statfd = open(statfile,'w')
	statfd.write('airmass = {0:0.2f}\r\n'.format(airmass))
	statfd.close()
	

def getairmass():
	for a in range(10):
		try:
			airmass = float( tel_slow.reqALL(timeout=1.0)['secz'] )
			return float( airmass )
		except Exception as err:
			print err, "trying again"

	return 1.0
		
		


fake_telstat()
