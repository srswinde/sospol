procedure telstat (dummy)

     string dummy

begin

     string getfile, statfile, stopfile, variable, dosfile
     bool run

     getfile = "getstat.loc"
     stopfile = "stopstat.loc"
     statfile = "status.tel"
     variable = "airmass"
     dosfile = mktemp("dos")

# Make sure we start out clean

     if (access(getfile)) delete (getfile, verify = no)
     if (access(stopfile)) delete (stopfile, verify = no)
     if (access(statfile)) delete (statfile, verify = no)

# Start infinite loop

     print ("Telescope status logging started...")

     run = yes
     while (run) {

# If getfile exists, check for non-existence of status file;
#    then write new status file
# When done, delete getfile to signal PC that status is ready

        if (access(getfile)) {
	    if (access(statfile)) {beep; print ("Status file exists!"); break}
	    telescope ( ) | match (variable, > statfile)
# Copy to second file so unix2dos works the same on Solaris and Linux machines
            copy (statfile, dosfile, verbose-) 
            print ("!unix2dos -n " // statfile // " " // dosfile // " >&/dev/null") | cl
	    delete (statfile, verify = no)
	    rename (dosfile, statfile)
	    print ("!chmod 777 " // statfile ) | cl
	    delete (getfile, verify = no)
	    }

# Wait one second

     sleep (1)

# If commanded, stop loop and clean up leftovers

	if (access(stopfile)) {
	    run = no
 	    delete (stopfile, verify = no)
	    if (access(getfile)) delete (getfile, verify = no)
	    if (access(statfile)) delete (statfile,verify = no)
	    beep; print ("Telescope status logging halted.")
	}

     }
	
end
